#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  miniTerm.py
#
#  Copyright 2020 mpho <mphoraff@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

__author__ = 'mpho'


# Importing the libraries
import sys, os
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
gi.require_version('Vte', '2.91')  # vte-0.38 (gnome-3.14)
from gi.repository import Vte, GLib, GObject


class Terminal(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="Hello Mini Terminal")
        self.set_default_size(600, 400)
        self.set_border_width(3)

        self.notebook = Gtk.Notebook()
        self.add(self.notebook)
        VTE_ADDITIONAL_WORDCHARS = "-,./?%&#:_";

        term1 = Vte.Terminal.new()
        term1.set_size(30, 3)
        term1.set_mouse_autohide(True)
        term1.set_scroll_on_output(True)
        term1.set_audible_bell(True)
        term1_pty = term1.pty_new_sync(Vte.PtyFlags.DEFAULT, None)
        term1.set_pty(term1_pty)
        term1.set_word_char_exceptions(VTE_ADDITIONAL_WORDCHARS)
        term1_fd = Vte.Pty.get_fd(term1_pty)
        term1.spawn_async(
                Vte.PtyFlags.DEFAULT, #default is fine
                os.environ['HOME'], #where to start the command?
                ["/bin/bash"], #where is the emulator?
                [""], #it's ok to leave this list empty
                GLib.SpawnFlags.DO_NOT_REAP_CHILD,
                None, -1, term1_fd)

        term2 = Vte.Terminal.new()
        term2.set_size(30, 3)
        term2.set_mouse_autohide(True)
        term2.set_scroll_on_output(True)
        term2_pty = term2.pty_new_sync(Vte.PtyFlags.DEFAULT, None)
        term2.set_pty(term2_pty)
        term2.set_word_char_exceptions(VTE_ADDITIONAL_WORDCHARS)
        term2_fd = Vte.Pty.get_fd(term2_pty)
        term2.spawn_async(
                Vte.PtyFlags.DEFAULT, #default is fine
                os.environ['HOME'], #where to start the command?
                ["/bin/bash"], #where is the emulator?
                [""], #it's ok to leave this list empty
                GLib.SpawnFlags.DO_NOT_REAP_CHILD,
                None, -1, term2_fd)

        scrollWin1 = Gtk.ScrolledWindow()
        scrollWin1.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
        scrollWin1.set_border_width(1)
        Gtk.Container.add(scrollWin1, term1)
        self.notebook.append_page(scrollWin1, Gtk.Label(label="Terminal 1"))

        scrollWin2 = Gtk.ScrolledWindow()
        scrollWin2.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
        scrollWin2.set_border_width(1)
        Gtk.Container.add(scrollWin2, term2)
        self.notebook.append_page(scrollWin2, Gtk.Label(label="Terminal 2"))

    # def on_button_clicked(self, widget):
        # print("button clicked Hello World")
        # print(dir(widget.props))


win = Terminal()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()

